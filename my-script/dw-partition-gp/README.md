## dw-partition-gp

Greenplum 数据仓库定时创建分区表程序

使用：

1. 先 mvn clean package 打包
2. 然后在 jar 包所在目录创建 application.properties 文件，并配置正确的数据源
3. 运行命令 java -jar xxxx.jar
