package me.w1992wishes.partition.mapper.odl;

import me.w1992wishes.partition.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @author w1992wishes 2019/3/20 16:55
 */
@Repository
public interface OdlMapper extends BaseMapper {

}
