# spark 学习例子

记录学习spark 的过程。

## 模块

* [etl 离线加载数据预处理](etl-offline-preprocess/README.md)
* [spark 例子](spark-example/README.md)
* [参考 spark kmeans 实现的分区聚类](spark-partitioning/README.md)